from pathlib import Path

import cv2
import numpy as np
import torch
import torch.nn as nn
from PIL import Image
from torchvision import models, transforms

from yoloface.utils import IMG_HEIGHT, IMG_WIDTH, CONF_THRESHOLD, NMS_THRESHOLD
from yoloface.utils import get_outputs_names, post_process

num_classes = 2
input_size = 224


def initialize_model(num_classes, use_pretrained=True):
    model_ft = models.squeezenet1_0(pretrained=use_pretrained)
    model_ft.classifier[1] = nn.Conv2d(512, num_classes, kernel_size=(1, 1), stride=(1, 1))
    model_ft.num_classes = num_classes
    return model_ft


def get_yoloface():
    class empty(object):
        def __init__(self):
            pass

    args = empty()
    args.model_cfg = './yoloface/cfg/yolov3-face.cfg'
    args.model_weights = './yoloface/model-weights/yolov3-wider_16000.weights'
    args.image = ''
    args.video = ''
    args.src = 0
    args.output_dir = 'yoloface/outputs/'

    net = cv2.dnn.readNetFromDarknet(args.model_cfg, args.model_weights)
    net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
    net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
    return net


def check_image(net, img):
    img_origin = img.copy()
    blob = cv2.dnn.blobFromImage(img, 1 / 255, (IMG_WIDTH, IMG_HEIGHT),
                                 [0, 0, 0], 1, crop=False)

    net.setInput(blob)

    outs = net.forward(get_outputs_names(net))

    faces = post_process(img, outs, CONF_THRESHOLD, NMS_THRESHOLD)

    faces = np.clip(faces, 0, 10000000)
    faces_detected = [
        [face[0], face[0] + face[2], face[1], face[1] + face[3]] for face in faces
    ]
    count_faces = 0
    for y1, y2, x1, x2 in faces_detected:
        sub_img = img_origin[x1: x2, y1: y2]
        sub_img = cv2.cvtColor(sub_img, cv2.COLOR_BGR2RGB)
        im_pil = Image.fromarray(sub_img)
        img = data_transforms['val'](im_pil)
        img = img.unsqueeze(0)
        prediction = model_ft(img)
        is_face_detected = prediction[0][0] > prediction[0][1]
        count_faces += int(is_face_detected)
    return count_faces


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

model_ft = initialize_model(num_classes, use_pretrained=False)
model_ft.load_state_dict(torch.load('SqueezeNet.pth'))
model_ft.eval()

data_transforms = {
    'val': transforms.Compose([
        transforms.Resize((input_size, input_size)),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
}

net = get_yoloface()

data_root = Path('/workspace/input_data')

for path in data_root.glob('*'):
    img = cv2.imread(str(path))
    try:
        count_faces = check_image(net, img)
    except Exception:
        # silently shut down the error
        # print(f'error {path}')
        continue

    if count_faces > 0:
        print(path.name)

