echo ${1}
docker build . -t face_noface_inference
docker run --rm -it -v ${1}:/workspace/input_data face_noface_inference bash -c "python inference.py"