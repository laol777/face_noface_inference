FROM pytorch/pytorch:1.4-cuda10.1-cudnn7-runtime

RUN apt update
RUN apt-get install -y libglib2.0-0 libsm6 libxext6 libxrender-dev
RUN pip install opencv-python opencv-contrib-python

RUN pip install tensorflow>=1.12.1 keras pillow matplotlib

RUN pip install ipdb

COPY . /workspace